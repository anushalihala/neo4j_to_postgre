from mappings import NEO4J_TO_RELATIONAL_MAPPING
import requests
import json
from main import Neo4J2Postgres

def lambda_handler(event, context):
    print(event)
    print(context)

    print("hello Lambda")
    n2p = Neo4J2Postgres(NEO4J_TO_RELATIONAL_MAPPING)

    n2p.transform_neo4j_to_postgres("manufacturer", "m")
    n2p.transform_neo4j_to_postgres("product", "p")
    n2p.transform_neo4j_to_postgres("update", "u")
    print(n2p.SQL_QUERIES)
    n2p.write_queries()

    n2p.neo4j_app.close()

    return {
        'statusCode': 200,
        'body': json.dumps("Work!! :D")
    }

