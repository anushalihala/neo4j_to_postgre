import boto3
import json

# def lambda_handler(event, context):
#     logging.getLogger().setLevel(logging.INFO)
#     string = "dfghj"
#     encoded_string = string.encode("utf-8")
#
#     bucket_name = "data-pipeline-sql-to-platform"
#     file_name = "anusha_hello.sql"
#     s3_path = "test-30-march-2022/" + file_name
#
#     s3 = boto3.resource("s3")
#     print('putting object...')
#     response = s3.Bucket(bucket_name).put_object(Key=s3_path, Body=encoded_string)
#     return json.dumps(response, default=str)


class S3App:
    def __init__(self):
        self.s3 = boto3.resource("s3")

    def put_object(self, bucket_name, path, file):
        self.s3 = boto3.resource("s3")
        print(f"Putting object at ... {bucket_name}:{path}")
        response = self.s3.Bucket(bucket_name).put_object(Key=path, Body=file)
        return json.dumps(response, default=str)


if __name__ == "__main__":
    lambda_handler(1,2)