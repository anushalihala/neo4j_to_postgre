"""
### Mapping Format ###

ASSUMPTION: TABLES PROCESSED IN ORDER

DATA_SOURCE (like Sony, B&H) = [
    {
        'postgres_table': name_of_the_postgres_table_where_insertion_will_be_made,
        'neo4j_object': neo4j_object_char,
        'mapping': {
            postgres_regular_column_name: corresponding_json_key_in_firestore_product_data,
            postgres_foreign_key_column_name:[foreign_key_column,
                                              foreign_key_table,
                                              foreign_key_column_to_match,
                                              value_of_firestore_json_field_required_for_matching],
        'RELATED_TABLES': {
            related_table_name: foreign_key_field_name
        },
        "FILTER_ON": [column1, column2, ...],
        "PRIMARY_KEY": primary_key_column
            .....
        }
    },

    {
        'postgres_table': 'name_of_the_postgres_table_in_which_insertion_will_be_made',
        'neo4j_object': neo4j_object_char,
        'mapping': {
            'postgres_column_name': 'corresponding_json_key_in_firestore_product_data',

            .....
        }
    },
    ..............
]

Note the the list mapping for foreign keys is used to execute queries:
The list ['id','manufacturer','name','Source'] would execute the following query:
SELECT id FROM manufacturer WHERE name = firestore_data['Source'];

"""

NEO4J_TO_RELATIONAL_MAPPING = [
    {
        'postgres_table': 'manufacturer',
        'neo4j_object': 'm',
        'mapping': {
            'name': 'manufacturer_name',
            'graph_id': 'graph_id'
        },
        "RELATED_TABLES": {
            "product": "manufacturerId"
        },
        "FILTER_ON": ["name"],
        "PRIMARY_KEY": "id"
    },

    {
        'postgres_table': 'product',
        'neo4j_object': 'p',
        'mapping': {
            'name': 'name',
            'manufacturerId': ['id', 'manufacturer', 'name', 'manufacturer_name'],
            'model': 'mpn_or_sku',
            'imageUrl': 'image_url',
            'pageUrl': 'url',
            'description': 'description',
            'graph_id': 'graph_id'
        },
        "FILTER_ON": ["model", "manufacturerId"],
        "PRIMARY_KEY": "id"
    },
    {
        'postgres_table': 'update',
        'neo4j_object': 'u',
        'mapping': {
            'originalMessage': 'update_page_url',
            'subject': 'update_name',
            'description': 'update_description',
            'productId': ['id', 'product', 'model', 'mpn_or_sku']
        },
        'FILTER_ON': ['originalMessage']
    },
]