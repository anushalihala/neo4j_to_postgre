from mappings import NEO4J_TO_RELATIONAL_MAPPING
from neo4j_app import Neo4jApp
from access_rds import RDSApp
from access_s3 import S3App
import json
from utils import get_current_date
from constants import DATA_MANUFACTURER


class Neo4J2Postgres:
    def __init__(self, mapping):
        self.mapping = mapping
        self.neo4j_app = Neo4jApp()
        self.postgres_delta_table = None
        """
        self.SQL_QUERIES = [
            {
                path: manufacturer/table_name/[insert|update]/postgres_id
                query: SQL QUERY
            }
        ]
        """
        self.SQL_QUERIES = []

    def write_queries(self):
        """
        Generated queries are written to a file.
        """

        print("Number of queries:", len(self.SQL_QUERIES))

        bucket_name = "data-pipeline-sql-to-platform"
        for query_object in self.SQL_QUERIES:
            query_path = query_object["path"]
            sql_transaction = "BEGIN\n" + query_object["query"] + "\nCOMMIT"
            s3 = S3App()
            s3.put_object(bucket_name, query_path, sql_transaction)


    def extract_postgres_records_for_table(self, table_mapping, table_objects):
        """
        Transform Neo4J objects into Postgres product list of dictionaries
        """
        # output initialisation
        postgres_delta_table = []

        # validation
        if table_objects is None:
            raise Exception("MappingError: table_objects argument for extract_postgres_records_for_table() is None")
        if table_mapping is None:
            raise Exception("MappingError: 'table_name' argument for extract_postgres_records_for_table() does not match any postgres table in mapping.py")

        for result in self.neo4j_app.result_objects[table_mapping["neo4j_object"]]:
            output_row = {}
            for postgres_field_name in table_mapping["mapping"].keys():
                neo4j_field_name = table_mapping["mapping"][postgres_field_name]

                if type(neo4j_field_name) is list:
                    # foreign key linking list
                    #   assign mapping list first
                    data_value = neo4j_field_name[:]
                    #   get neo4j field name from mapping list
                    field_name_from_list = neo4j_field_name[-1]
                    #   assign neo4j field value to end of data_value list

                    if field_name_from_list not in result:
                        # current record does not contain field
                        continue

                    data_value[-1] = result[field_name_from_list]
                else:
                    if neo4j_field_name not in result:
                        # current record does not contain field
                        continue

                    data_value = result[neo4j_field_name]

                output_row[postgres_field_name] = data_value

            postgres_delta_table.append(output_row)

        return postgres_delta_table

    def aws_rds_select(self, sql_query):
        """
        returns relational object {'id': [___], 'record': [___] }
        """
        rds = RDSApp()
        select_results = rds.rds_select(sql_query)
        rds.close_connection()
        if len(select_results["rows"]) > 1:
            raise Exception(
                        f"MappingError: SQL Query '{sql_query}' returned more than one ID")
        elif len(select_results["rows"]) == 0:
            # raise Exception(
            #     f"MappingError: SQL Query '{sql_query}' returned no IDs")
            return {"id":None, "record":None}
        else:
            # TODO: generalise 'id' later
            return {"id": select_results["rows"][0][0], "record": select_results["rows"][0]}

    def aws_rds_insert_or_update(self, query, args):
        rds = RDSApp()
        rds.rds_insert_or_update(query, args)
        rds.close_connection()

    def generate_insert_query_for_psycopg2(self, record, table_mapping):
        # TODO
        pass

    def generate_update_query_for_psycopg2(self, record, table_mapping):
        update_set_string_list = []
        for key in record.keys():
            curr_update_field = json.dumps(key).replace("'", '"')
            update_set_string_list.append(f"{table_mapping['postgres_table']}.{curr_update_field} = %s")

        update_set_string_list.append('product."updatedAt" = transaction_timestamp()')
        update_set_string = ", ".join(update_set_string_list)
        print("Update set string", update_set_string)

        sql_query = f'''
UPDATE {table_mapping['postgres_table']} 
SET {update_set_string}
WHERE {table_mapping['postgres_table']}.{json.dumps(table_mapping['PRIMARY_KEY'])} = %s
'''
        return sql_query

    def generate_insert_query_for_transaction(self, record, table_mapping):
        # add timestamps
        insert_columns_list = list(record.keys())
        insert_columns_list.append('createdAt')
        insert_columns_list.append('updatedAt')
        insert_columns_string = ", ".join(insert_columns_list)
        jsonifyed_record_values_list = [json.dumps(val).replace('"', "'") for val in record.values()]

        jsonifyed_record_values_list.append('transaction_timestamp()')
        jsonifyed_record_values_list.append('transaction_timestamp()')
        insert_values_string = ", ".join(jsonifyed_record_values_list)
        print("Insert values list", insert_values_string)

        sql_query = f'''
INSERT INTO {table_mapping['postgres_table']}({insert_columns_string}) VALUES({insert_values_string})
'''
        return sql_query

    def generate_update_query_for_transaction(self, record, table_mapping, id):
        update_set_string_list = []
        for key in record.keys():
            curr_update_field = json.dumps(key)
            curr_update_value = json.dumps(record[key]).replace('"', "'")
            update_set_string_list.append(f"{table_mapping['postgres_table']}.{curr_update_field} = {curr_update_value}")

        update_set_string_list.append('product."updatedAt" = transaction_timestamp()')
        update_set_string = ", ".join(update_set_string_list)
        print(record)
        id_string = json.dumps(id).replace('"', "'")

        sql_query = f'''
UPDATE {table_mapping['postgres_table']} 
SET {update_set_string}
WHERE {table_mapping['postgres_table']}.{json.dumps(table_mapping['PRIMARY_KEY'])} = {id_string}
'''
        return sql_query

    def generate_delete_query_for_transaction(self, record_id, table_mapping):
        id_json = json.dumps(table_mapping["PRIMARY_KEY"])
        record_id_json = json.dumps(record_id).replace('"', "'")
        sql_query = f'''
DELETE FROM {table_mapping['postgres_table']}
WHERE {table_mapping['postgres_table']}.{id_json} = {record_id_json}
'''
        return sql_query

    def generate_sql_queries_for_table(self, table_mapping, delta_table):
        """
        Generate SQL queries for delta_table using table_mapping
        """
        one_to_many_maps = {}
        one_to_one_maps = {}

        for key in table_mapping["mapping"].keys():
            if type(table_mapping["mapping"][key]) is list:
                one_to_many_maps[key] = table_mapping["mapping"][key]
            else:
                one_to_one_maps[key] = table_mapping["mapping"][key]

        for record in delta_table:
            # jsonifyed_record = {}

            # DEAL WITH RELATIONSHIPS (LISTS)
            for key in one_to_many_maps.keys():
                if key not in record:
                    continue
                related_table_filter_value = "'" + str(record[key][3]) + "'"
                select_sql_for_related = f"""
SELECT {record[key][0]} FROM {record[key][1]}
WHERE {record[key][2]} = {related_table_filter_value}
"""
                select_result = self.aws_rds_select(select_sql_for_related)
                related_id = select_result["id"]
                select_record = select_result["record"]

                print("Related table SELECT query...")
                print(select_sql_for_related)
                print("ID found:")
                print(related_id)
                print("Selected record:")
                print(select_record)

                if related_id is None:
                    # raise Exception(
                    #     f"MappingError: No record found WHERE {record[key][2]} = {json.dumps(record[key][3])} for TABLE '{record[key][1]}'")
                    # TODO: Insert query using psycopg2
                    for current_mapping in self.mapping:
                        if current_mapping['postgres_table'] == record[key][1]:
                            related_mapping = current_mapping
                            break

                    self.transform_neo4j_to_postgres(related_mapping['postgres_table'], related_mapping['neo4j_object'])
                else:
                    record[key] = related_id

            # DEAL WITH MAIN TABLE
            for key in one_to_one_maps.keys():
                if key not in record:
                    continue

            filter_string_list = []
            for filter_field in table_mapping["FILTER_ON"]:
                if filter_field in record:
                    if filter_field in one_to_many_maps:
                        filter_field_value = record[filter_field]
                    else:
                        filter_field_value = record[filter_field]
                    filter_field_name = json.dumps(filter_field)
                    filter_field_value = json.dumps(filter_field_value).replace('"', "'")
                    filter_string_list.append(f"{table_mapping['postgres_table']}.{filter_field_name} = {filter_field_value}")

            filter_string = " AND ".join(filter_string_list)
            print()
            print("Filter clause..")
            print(filter_string)
            print()

            # SELECT FIRST TO AVOID DUPLICATES
            select_sql_query = f'''
SELECT * FROM {table_mapping['postgres_table']}
WHERE {filter_string}
'''

            select_result = self.aws_rds_select(select_sql_query)
            main_id = select_result["id"]
            main_select_record = select_result["record"]

            print("Main table SELECT query...")
            print(select_sql_query)
            print("ID found:")
            print(main_id)
            print("Main Select Record:")
            print(main_select_record)
            print()

            # if True: # for testing
            if main_id is None:
                # INSERT
                current_date = get_current_date()
                query = self.generate_insert_query_for_transaction(record, table_mapping)
                print(query)
                sql_query_object = {
                    "path": f"{DATA_MANUFACTURER}/{table_mapping['postgres_table']}/INSERT/{current_date}.sql",
                    "query": query
                }
                self.SQL_QUERIES.append(sql_query_object)
                undo_query = self.generate_delete_query_for_transaction(main_id, table_mapping)
                print("UNDO SQL:", undo_query)
                undo_query_object = {
                    "path": f"{DATA_MANUFACTURER}/{table_mapping['postgres_table']}/INSERT_UNDO/{current_date}.sql",
                    "query": undo_query
                }
                self.SQL_QUERIES.append(undo_query_object)
            else:
                # UPDATE
                current_date = get_current_date()
                query = self.generate_update_query_for_transaction(record, table_mapping, main_id)
                print(query)
                sql_query_object = {
                    "path": f"{DATA_MANUFACTURER}/{table_mapping['postgres_table']}/UPDATE/{current_date}.sql",
                    "query": query
                }
                self.SQL_QUERIES.append(sql_query_object)
                # UPDATE UNDO
                undo_query = self.generate_update_query_for_transaction(main_select_record, table_mapping, main_id)
                print("Undo SQL:", undo_query)
                undo_query_object = {
                    "path": f"{DATA_MANUFACTURER}/{table_mapping['postgres_table']}/UPDATE_UNDO/{current_date}.sql",
                    "query": undo_query
                }
                self.SQL_QUERIES.append(undo_query_object)


    def transform_neo4j_to_postgres(self, postgres_table_name, neo4j_object_name):
        # using corresponding table mapping
        table_mapping = None
        for curr_table_mapping in self.mapping:
            if curr_table_mapping["postgres_table"] == postgres_table_name:
                table_mapping = curr_table_mapping
                break

        self.neo4j_app.get_object(postgres_table_name)
        neo4j_objects = self.neo4j_app.result_objects[neo4j_object_name]
        self.neo4j_app.write_to_s3()
        postgres_delta_table = self.extract_postgres_records_for_table(table_mapping, neo4j_objects)
        self.generate_sql_queries_for_table(table_mapping, postgres_delta_table[0:1])


if __name__ == '__main__':
    print("hello main")
    n2p = Neo4J2Postgres(NEO4J_TO_RELATIONAL_MAPPING)

    n2p.transform_neo4j_to_postgres("manufacturer", "m")
    n2p.transform_neo4j_to_postgres("product", "p")
    n2p.transform_neo4j_to_postgres("update", "u")
    print(n2p.SQL_QUERIES)
    n2p.write_queries()

    n2p.neo4j_app.close()

