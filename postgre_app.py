import os
import psycopg2 as pg


class PostgresConn:
    """
    This class:
        1. Connects to AWS RDS Beam Databse
        2. Executes Write Commands via the Connection
        3. Executes Read Commnands via the Connection
    """

    def __init__(self):
        try:
            self.connection = pg.connect(
                database=os.environ['DB_NAME'],
                host=os.environ['DB_HOST'],
                port=os.environ['DB_PORT'],
                user=os.environ['DB_USER'],
                password=os.environ['DB_PASS'],
            )
            self.cursor = self.connection.cursor()
            print('Connected to Database')

        except Exception as e:
            print(e)
            print('Connection Has Failed...')

    def get_cursor(self):
        return self.cursor()

    def execute_query(self, query):
        try:
            self.cursor.execute(query)
            self.connection.commit()
            return True
        except Exception as e:
            print(f"The error '{e}' occurred")

    def read_query(self, query):
        try:
            self.cursor.execute(query)
            r = self.cursor.fetchall()
            return r
        except Exception as e:
            print(f"The error '{e}' occurred")


if __name__ == "__main__":
    print("hello PostgreSQL")
    pc = PostgresConn()
    result = pc.read_query("SELECT id FROM product LIMIT 1")
    print(result)
