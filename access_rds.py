# Author Victor Polo; Extension Anusha Lihala
# Demo with Best Practices to connect to the sandboxstudio database

import json
import psycopg2
import psycopg2.extras
import os
import datetime


class RDSApp:
    def __init__(self):
        db_name = os.environ.get('DB_NAME')
        db_user = os.environ.get('DB_USER')
        db_host = os.environ.get('DB_HOST')
        db_port = os.environ.get('DB_PORT')
        db_pass = os.environ.get('DB_PASS')
        self.conn = psycopg2.connect(user=db_user, database=db_name, host=db_host,
                                     password=db_pass, port=db_port)

        self.cursor = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

    def rds_select(self, query):
        # execute sql
        print("Executing following SQL SELECT query...")
        print(query)
        self.cursor.execute(query)

        # get column names
        column_names = [desc[0] for desc in self.cursor.description]
        rows = self.cursor.fetchall()
        for row in rows:
            for i, field in enumerate(row):
                if isinstance(field, datetime.datetime):
                    print(f"Converting {field} to ISO datetime..")
                    row[i] = row[i].isoformat()

        output = {"header": column_names, "rows": rows}

        return output

    def rds_insert_or_update(self, insert_query, row_to_insert, select_query):
        print("Executing following SQL SELECT query...")
        print(insert_query)
        print("Row to insert...")
        print(row_to_insert)
        self.cursor.execute(insert_query, row_to_insert)

        print("Verification...")
        verify_result = self.rds_select(select_query)
        print("Select result...")
        print(verify_result)

    def close_connection(self):
        self.cursor.close()


if __name__ == "__main__":
    print("hello Postgres")
    rds = RDSApp()
    result = rds.rds_select("select * from product limit 1")
    print(result)
    rds.rds_insert("INSERT INTO manufacturer(name) VALUES(%s)", ("Sony",), "SELECT * FROM manufacturer WHERE name='Sony'")

