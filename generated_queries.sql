BEGIN



INSERT INTO manufacturer(name) VALUES('Datavideo')


INSERT INTO product(manufacturerId, name, model, imageUrl, pageUrl, description) VALUES(123, '50M Intercom Extension Cable', 'CB-4', 'https://d3c9kujynjspib.cloudfront.net/images/8f32e3c80293c9238747619d.jpg', 'https://www.datavideo.com/us/product/CB-4', 'The <b>CB-4</b> from <b>Datavideo</b> is a 50 meter extension cable intended for use with the ITC-100 intercom system.  It has a 5-pin male XLR connector at one end, and a 5-pin female XLR connector at the other.')
