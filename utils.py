import datetime


def get_current_date():
    date_string = datetime.datetime.now().isoformat()
    return date_string


if __name__ == "__main__":
    print(get_current_date())