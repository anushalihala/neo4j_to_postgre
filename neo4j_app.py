import json
import os
import datetime
from neo4j import GraphDatabase
from neo4j.exceptions import CypherSyntaxError
import logging
from neo4j.exceptions import ServiceUnavailable
from constants import DATA_MANUFACTURER
from utils import get_current_date
from access_s3 import S3App


class Neo4jApp:
    def __init__(self):
        self.uri = 'neo4j+s://6f368194.databases.neo4j.io'
        self.user = 'neo4j'
        self.password = os.environ.get("NEO4J_PASSWORD")
        self.driver = GraphDatabase.driver(self.uri, auth=(self.user, self.password))
        self.result_objects = {}

        """
        self.QUERIES[object_type_name] = {
            "query": ( QUERY ),
            "returned_objects": [a, b, c..] # Here 'a' is primary object
        }
        """
        self.QUERIES = {}
        self.QUERIES["product"] = {
            "query": (
                "MATCH(p:PRODUCT)-[:Manufacturer]->(m:MANUFACTURER{manufacturer_name:\"" + DATA_MANUFACTURER + "\"} ) "
                "MATCH(p)-[:Source]->(s:SOURCE{source_name:\"aggregate\"}) "
                "WHERE EXISTS( (p)-[:Import_Status]->(:IMPORT_STATUS{import_status_name:\"TO_IMPORT\"}) ) "
                "RETURN p, m "
            ),
            "returned_objects": ["p", "m"]
        }
        self.QUERIES["manufacturer"] = {
            "query": (
                "MATCH (m:MANUFACTURER{manufacturer_name:\"" + DATA_MANUFACTURER + "\"}) "
                "WHERE EXISTS( (m)-[:Import_Status]->(:IMPORT_STATUS{import_status_name:\"TO_IMPORT\"}) ) "
                "RETURN m"
            ),
            "returned_objects": ["m"]
        }
        # TODO: Remove hard-coding
        self.QUERIES["update"] = {
            "query": (
                "MATCH (p:PRODUCT)-[:Update]->(u:UPDATE)"
                "MATCH (p)-[:Source]->(s:SOURCE{source_name:\"" + "sony" + "\"})"
                "MATCH (p)-[:Manufacturer]->(m:MANUFACTURER{manufacturer_name:\"" + "Sony" + "\"})"
                "RETURN u"
            ),
            "returned_objects": ["u"]
        }

    def close(self):
        # Don't forget to close the driver connection when you are finished with it
        self.driver.close()

    def find_and_return_to_import_object(self, query, returned_objects):
        object_char = returned_objects[0]
        with self.driver.session() as session:
            self.result_objects[object_char] = session.read_transaction(self._find_and_return_to_import_object, query, returned_objects)

    @staticmethod
    def _find_and_return_to_import_object(tx, query, returned_objects):
        results = tx.run(query)

        result_objects = []

        for row in results:
            output_row = {}
            for obj in returned_objects:
                for key in row[obj].keys():
                    if "DATE" in key:
                        date = row[obj][key]
                        output_row[key] = str(date.year) + "-" + str(date.month) + "-" + str(date.day)
                    else:
                        output_row[key] = row[obj][key]

            result_objects.append(output_row)

        return result_objects

    def get_object(self, object_name):
        try:
            self.find_and_return_to_import_object(self.QUERIES[object_name]["query"],
                                                  self.QUERIES[object_name]["returned_objects"])
        except CypherSyntaxError:
            print('Syntax error in below query:')
            print(self.QUERIES[object_name]["query"])

    def write_to_s3(self):
        if self.result_objects is None:
            print("No Neo4J JSON data to write")

        bucket_name = "data-pipeline-sql-to-platform-logs"
        current_date = get_current_date()
        path = f"{DATA_MANUFACTURER}/neo4j_json/{current_date}.json"

        s3 = S3App()
        s3.put_object(bucket_name, path, json.dumps(self.result_objects, indent=3))


if __name__ == "__main__":
    nap = Neo4jApp()
    print(nap.uri)
    print(nap.user)
    print(nap.password)

    nap.get_object("manufacturer")
    print(nap.result_objects["m"])
    nap.get_object("product")
    print(nap.result_objects["p"][0])
    print(len(nap.result_objects["p"]))
    nap.write_to_s3()
    nap.close()
    print("done")